jjk 

# --------------
# Please enter the commit message for your changes. Everything below
# this paragraph is ignored, and an empty message aborts the commit.
# Just close the window to accept your message.
diff --git a/utils/FISTA.m b/utils/FISTA.m
deleted file mode 100644
index fe3e60c..0000000
--- a/utils/FISTA.m
+++ /dev/null
@@ -1,94 +0,0 @@
-function [X, iter] = fista(grad, Xinit, L, lambda, opts, calc_F)   
-% * A Fast Iterative Shrinkage-Thresholding Algorithm for 
-% Linear Inverse Problems.
-% * Solve the problem: `X = arg min_X F(X) = f(X) + lambda||X||_1` where:
-%   - `X`: variable, can be a matrix.
-%   - `f(X)` is a smooth convex function with continuously differentiable 
-%       with Lipschitz continuous gradient `L(f)` (Lipschitz constant of 
-%       the gradient of `f`).
-% * Syntax: `[X, iter] = FISTA(calc_F, grad, Xinit, L, lambda,  opts)` where:
-%   - INPUT:
-%     + `grad`: a _function_ calculating gradient of `f(X)` given `X`.
-%     + `Xinit`: initial guess.
-%     + `L`: the Lipschitz constant of the gradient of `f(X)`.
-%     + `lambda`: a regularization parameter, can be either a scalar or 
-%       a weighted matrix.
-%     + `opts`: a _structure_ variable describing the algorithm.
-%       * `opts.max_iter`: maximum iterations of the algorithm. 
-%           Default `300`.
-%       * `opts.tol`: a tolerance, the algorithm will stop if difference 
-%           between two successive `X` is smaller than this value. 
-%           Default `1e-8`.
-%       * `opts.show_progress`: showing `F(X)` after each iteration or not. 
-%           Default `false`. 
-%     + `calc_F`: optional, a _function_ calculating value of `F` at `X` 
-%       via `feval(calc_F, X)`. 
-% ---------------------------
-% Author: Tiep Vu, 4/6/2016 
-% ---------------------------
-%% 
-    if ~exist('opts', 'var')
-        opts.max_iter = 300;
-        opts.tol = 1e-8;
-        opts.show_progress = false;
-    else
-        if ~isfield(opts, 'tol')
-            opts.tol = 1e-8;
-        end 
-        if ~isfield(opts, 'max_iter');
-            opts.max_iter = 300;
-        end 
-        if ~isfield(opts, 'show_progress');
-            opts.show_progress = false;
-        end           
-    end    
-    
-    if opts.show_progress        
-        fprintf('(max_iter = %d): 0.\n', opts.max_iter);
-    end     
-    %%
-    Linv = 1/L;    
-    lambdaLiv = lambda*Linv;
-    x_old = Xinit;
-    y_old = Xinit;
-    t_old = 1;
-    iter = 0;
-    cost_old = 1e10;
-    %% MAIN LOOP
-    while  iter < opts.max_iter
-        iter = iter + 1;
-        x_new = shrinkage(y_old - Linv*feval(grad, y_old), lambdaLiv);
-        t_new = 0.5*(1 + sqrt(1 + 4*t_old^2));
-        y_new = x_new + (t_old - 1)/t_new * (x_new - x_old);
-        %% check stop criteria
-        e = norm1(x_new - x_old)/numel(x_new);
-        if e < opts.tol
-            break;
-        end
-        %% update
-        x_old = x_new;
-        t_old = t_new;
-        y_old = y_new;
-        %% show progress
-        if opts.show_progress
-            if nargin ~= 0
-                cost_new = feval(calc_F, x_new);
-                if cost_new <= cost_old 
-                    stt = 'YES.';
-                else 
-                    stt = 'NO, check your code.';
-                end
-                fprintf('iter = %3d, cost = %d, cost decreases? %s\n', ...
-                    iter, cost_new, stt);
-            else 
-                if mod(iter, 5) == 0
-                    fprintf('.');
-                end
-                if mod(iter, 10) == 0 
-                   fprintf('%d', iter);
-                end     
-            end        
-        end 
-    end
-    X = x_new;
-end 
\ No newline at end of file
diff --git a/utils/FISTA_test.m b/utils/FISTA_test.m
deleted file mode 100644
index 3533c27..0000000
--- a/utils/FISTA_test.m
+++ /dev/null
@@ -1,56 +0,0 @@
-function FISTA_test()
-    warning off all;
-    addpath(fullfile('..', 'utils'));
-    addpath(fullfile('..', 'build'));
-    %% ========= Test mode (if nargin == 0) ==============================
-    d      = 5; % data dimension
-    N      = 10; % number of samples 
-    k      = 10; % dictionary size 
-    lambda = 0.1;
-    Y      = normc(rand(d, N));
-    D      = normc(rand(d, k));
-    Xinit  = zeros(size(D,2), size(Y, 2));
-    %
-    opts.max_iter     = 300;
-    opts.show_progress = 0;
-    opts.checkgrad    = 0;  
-    % param for mex
-    param.lambda     = lambda; % not more than 20 non-zeros coefficients
-    param.lambda2    = 0;
-    param.numThreads = 1; % number of processors/cores to use; the default choice is -1
-    param.mode       = 2;        % penalized formulation
-    % mex solution and optimal value 
-    addpath(fullfile('..', 'build_spams' ));
-    X0       = mexLasso(Y, D, param); 
-    costmex = calc_F(X0)  
-    %%
-%     profile off;
-%     profile on;
-    %% 
-    function cost = calc_f(X)
-        cost = 1/2 *normF2(Y - D*X);
-    end 
-    %% cost function 
-    function cost = calc_F(X) 
-        cost = calc_f(X) + lambda*norm1(X);
-    end 
-    %%
-    DtD = D'*D;
-    DtY = D'*Y;
-    function res = grad(X) 
-        res = DtD*X - DtY;
-    end 
-    %% Checking gradient 
-    check_grad(@calc_f, @grad, Xinit);
-    %%
-    L = max(eig(DtD));
-    opts.tol = 1e-10;
-    opts.max_iter = 500;
-    [X] = fista(@grad, Xinit, L, lambda, opts);
-%     profile viewer;
-    norm(X - X0)
-    costfista = calc_F(X);
-    costfista - costmex
-    
-
-end 
\ No newline at end of file